class Field {

  String nameField;
  String type;
  List<String> fieldsEnum; // Apenas para enums

  String extractor;

  Field({
    this.nameField,
    this.type,
    this.fieldsEnum,
    this.extractor
  });

}