import 'field.dart';

class Group {

  String nameClass;
  String nameFileClass;
  int startLine;
  int endLine;
  List<Field> fields;

  Group({
    this.nameClass,
    this.nameFileClass,
    this.startLine,
    this.endLine,
    this.fields
  });

}