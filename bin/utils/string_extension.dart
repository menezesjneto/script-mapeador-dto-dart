extension StringExtension on String {
    String capitalizeLower() {
      return "${this[0].toLowerCase()}${this.substring(1)}";
    }
    String capitalize() {
      return "${this[0].toUpperCase()}${this.substring(1)}";
    }
}