class ReuniaoModel {
  AtaModel atas;
  List<PessoaModel> convidados;
  DateTime data;
  String descricao;
  EmpresaModel empresa;
  EnderecoModel endereco;
  int id;
  double latitude;
  String local;
  double longitude;
  String responsavel;
  String titulo;

  ReuniaoModel   ({
    this.atas,
    this.convidados,
    this.data,
    this.descricao,
    this.empresa,
    this.endereco,
    this.id,
    this.latitude,
    this.local,
    this.longitude,
    this.responsavel,
    this.titulo,
  });

  factory ReuniaoModel.fromJson(Map<String, dynamic> json) =>  ReuniaoModel(
    atas: AtaModel.fromJson(json['atas']),
    convidados: PessoaModel.listFromJson(json['convidados']),
    data: Extractor.extractDate(json['data']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    endereco: EnderecoModel.fromJson(json['endereco']),
    id: json['id'],
    latitude: Extractor.extractDouble(json['latitude']),
    local: Extractor.extractString(json['local'], ''),
    longitude: Extractor.extractDouble(json['longitude']),
    responsavel: Extractor.extractString(json['responsavel'], ''),
    titulo: Extractor.extractString(json['titulo'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'atas': atas.toMap(),
    'convidados': ReuniaoModel.listToMap(convidados),
    'data': data,
    'descricao': descricao,
    'empresa': empresa.toMap(),
    'endereco': endereco.toMap(),
    'id': id,
    'latitude': latitude,
    'local': local,
    'longitude': longitude,
    'responsavel': responsavel,
    'titulo': titulo,
  };

  static List<ReuniaoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ReuniaoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ReuniaoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}