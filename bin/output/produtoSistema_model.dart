class ProdutoSistemaModel {
  CategoriaProdutoModel categoria;
  DateTime criado;
  String descricao;
  int id;
  String nome;

  ProdutoSistemaModel   ({
    this.categoria,
    this.criado,
    this.descricao,
    this.id,
    this.nome,
  });

  factory ProdutoSistemaModel.fromJson(Map<String, dynamic> json) =>  ProdutoSistemaModel(
    categoria: CategoriaProdutoModel.fromJson(json['categoria']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'categoria': categoria.toMap(),
    'criado': criado,
    'descricao': descricao,
    'id': id,
    'nome': nome,
  };

  static List<ProdutoSistemaModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ProdutoSistemaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ProdutoSistemaModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}