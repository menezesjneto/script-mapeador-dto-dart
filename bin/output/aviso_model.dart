class AvisoModel {
  bool ativo;
  List<CategoriaEmpresaModel> categorias;
  List<ComunidadeModel> comunidades;
  List<CooperadoModel> cooperados;
  DateTime criado;
  String descricao;
  List<EmpresaModel> empresas;
  List<EstadoModel> estados;
  List<FuncionarioModel> funcionarios;
  int id;
  List<MunicipioModel> municipios;
  String titulo;

  AvisoModel   ({
    this.ativo,
    this.categorias,
    this.comunidades,
    this.cooperados,
    this.criado,
    this.descricao,
    this.empresas,
    this.estados,
    this.funcionarios,
    this.id,
    this.municipios,
    this.titulo,
  });

  factory AvisoModel.fromJson(Map<String, dynamic> json) =>  AvisoModel(
    ativo: Extractor.extractBool(json['ativo']),
    categorias: CategoriaEmpresaModel.listFromJson(json['categorias']),
    comunidades: ComunidadeModel.listFromJson(json['comunidades']),
    cooperados: CooperadoModel.listFromJson(json['cooperados']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresas: EmpresaModel.listFromJson(json['empresas']),
    estados: EstadoModel.listFromJson(json['estados']),
    funcionarios: FuncionarioModel.listFromJson(json['funcionarios']),
    id: json['id'],
    municipios: MunicipioModel.listFromJson(json['municipios']),
    titulo: Extractor.extractString(json['titulo'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'ativo': ativo,
    'categorias': AvisoModel.listToMap(categorias),
    'comunidades': AvisoModel.listToMap(comunidades),
    'cooperados': AvisoModel.listToMap(cooperados),
    'criado': criado,
    'descricao': descricao,
    'empresas': AvisoModel.listToMap(empresas),
    'estados': AvisoModel.listToMap(estados),
    'funcionarios': AvisoModel.listToMap(funcionarios),
    'id': id,
    'municipios': AvisoModel.listToMap(municipios),
    'titulo': titulo,
  };

  static List<AvisoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => AvisoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<AvisoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}