class FamiliaProdutoModel {
  DateTime criado;
  String descricao;
  EmpresaModel empresa;
  int id;
  String nome;

  FamiliaProdutoModel   ({
    this.criado,
    this.descricao,
    this.empresa,
    this.id,
    this.nome,
  });

  factory FamiliaProdutoModel.fromJson(Map<String, dynamic> json) =>  FamiliaProdutoModel(
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'criado': criado,
    'descricao': descricao,
    'empresa': empresa.toMap(),
    'id': id,
    'nome': nome,
  };

  static List<FamiliaProdutoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => FamiliaProdutoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<FamiliaProdutoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}