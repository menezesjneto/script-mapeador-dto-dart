class EnderecoModel {
  String bairro;
  String celular;
  String cep;
  String cidade;
  String complemento;
  ComunidadeModel comunidade;
  PessoaModel cooperado;
  DateTime criado;
  String email;
  EmpresaModel empresa;
  String enderecoResidencial;
  int id;
  double latitude;
  double longitude;
  MunicipioModel municipio;
  String nome;
  String nomeRecado;
  String numero;
  String perimetro;
  bool quitada;
  String telefoneResidencial;
  String tempoResidencia;
  String uf;

  EnderecoModel   ({
    this.bairro,
    this.celular,
    this.cep,
    this.cidade,
    this.complemento,
    this.comunidade,
    this.cooperado,
    this.criado,
    this.email,
    this.empresa,
    this.enderecoResidencial,
    this.id,
    this.latitude,
    this.longitude,
    this.municipio,
    this.nome,
    this.nomeRecado,
    this.numero,
    this.perimetro,
    this.quitada,
    this.telefoneResidencial,
    this.tempoResidencia,
    this.uf,
  });

  factory EnderecoModel.fromJson(Map<String, dynamic> json) =>  EnderecoModel(
    bairro: Extractor.extractString(json['bairro'], ''),
    celular: Extractor.extractString(json['celular'], ''),
    cep: Extractor.extractString(json['cep'], ''),
    cidade: Extractor.extractString(json['cidade'], ''),
    complemento: Extractor.extractString(json['complemento'], ''),
    comunidade: ComunidadeModel.fromJson(json['comunidade']),
    cooperado: PessoaModel.fromJson(json['cooperado']),
    criado: Extractor.extractDate(json['criado']),
    email: Extractor.extractString(json['email'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    enderecoResidencial: Extractor.extractString(json['enderecoResidencial'], ''),
    id: json['id'],
    latitude: Extractor.extractDouble(json['latitude']),
    longitude: Extractor.extractDouble(json['longitude']),
    municipio: MunicipioModel.fromJson(json['municipio']),
    nome: Extractor.extractString(json['nome'], ''),
    nomeRecado: Extractor.extractString(json['nomeRecado'], ''),
    numero: Extractor.extractString(json['numero'], ''),
    perimetro: Extractor.extractString(json['perimetro'], ''),
    quitada: Extractor.extractBool(json['quitada']),
    telefoneResidencial: Extractor.extractString(json['telefoneResidencial'], ''),
    tempoResidencia: Extractor.extractString(json['tempoResidencia'], ''),
    uf: Extractor.extractString(json['uf'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'bairro': bairro,
    'celular': celular,
    'cep': cep,
    'cidade': cidade,
    'complemento': complemento,
    'comunidade': comunidade.toMap(),
    'cooperado': cooperado.toMap(),
    'criado': criado,
    'email': email,
    'empresa': empresa.toMap(),
    'enderecoResidencial': enderecoResidencial,
    'id': id,
    'latitude': latitude,
    'longitude': longitude,
    'municipio': municipio.toMap(),
    'nome': nome,
    'nomeRecado': nomeRecado,
    'numero': numero,
    'perimetro': perimetro,
    'quitada': quitada,
    'telefoneResidencial': telefoneResidencial,
    'tempoResidencia': tempoResidencia,
    'uf': uf,
  };

  static List<EnderecoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => EnderecoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<EnderecoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}