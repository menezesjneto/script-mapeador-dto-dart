class BannerModel {
  bool ativo;
  List<CategoriaEmpresaModel> categoriasEmpresas;
  DateTime criado;
  String descricao;
  bool disparadoPush;
  DateTime disparadoPushTimestamp;
  bool enviarPush;
  DateTime fimTimestamp;
  int id;
  String imagemUrl;
  DateTime inicioTimestamp;
  String linkExterno;
  String linkVideo;
  bool rascunho;
  String titulo;

  BannerModel   ({
    this.ativo,
    this.categoriasEmpresas,
    this.criado,
    this.descricao,
    this.disparadoPush,
    this.disparadoPushTimestamp,
    this.enviarPush,
    this.fimTimestamp,
    this.id,
    this.imagemUrl,
    this.inicioTimestamp,
    this.linkExterno,
    this.linkVideo,
    this.rascunho,
    this.titulo,
  });

  factory BannerModel.fromJson(Map<String, dynamic> json) =>  BannerModel(
    ativo: Extractor.extractBool(json['ativo']),
    categoriasEmpresas: CategoriaEmpresaModel.listFromJson(json['categoriasEmpresas']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    disparadoPush: Extractor.extractBool(json['disparadoPush']),
    disparadoPushTimestamp: Extractor.extractDate(json['disparadoPushTimestamp']),
    enviarPush: Extractor.extractBool(json['enviarPush']),
    fimTimestamp: Extractor.extractDate(json['fimTimestamp']),
    id: json['id'],
    imagemUrl: Extractor.extractString(json['imagemUrl'], ''),
    inicioTimestamp: Extractor.extractDate(json['inicioTimestamp']),
    linkExterno: Extractor.extractString(json['linkExterno'], ''),
    linkVideo: Extractor.extractString(json['linkVideo'], ''),
    rascunho: Extractor.extractBool(json['rascunho']),
    titulo: Extractor.extractString(json['titulo'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'ativo': ativo,
    'categoriasEmpresas': BannerModel.listToMap(categoriasEmpresas),
    'criado': criado,
    'descricao': descricao,
    'disparadoPush': disparadoPush,
    'disparadoPushTimestamp': disparadoPushTimestamp,
    'enviarPush': enviarPush,
    'fimTimestamp': fimTimestamp,
    'id': id,
    'imagemUrl': imagemUrl,
    'inicioTimestamp': inicioTimestamp,
    'linkExterno': linkExterno,
    'linkVideo': linkVideo,
    'rascunho': rascunho,
    'titulo': titulo,
  };

  static List<BannerModel> listFromJson(List<dynamic> data) {
    return data.map((post) => BannerModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<BannerModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}