class ProdutoModel {
  bool aceitaBoleto;
  bool aceitaCartaoExterno;
  double altura;
  bool ativo;
  CategoriaProdutoModel categoria;
  EmpresaModel clientePrincipal;
  String codigoEAN;
  String codigoNCM;
  double comprimento;
  DateTime criado;
  String descricao;
  DateTime disponivelLimiteTimestamp;
  EmpresaModel empresa;
  bool entregaGratis;
  FamiliaProdutoModel familia;
  EmpresaModel fornecedorPrincipal;
  String fotoPrincipalUrl;
  int id;
  double largura;
  int maximoParcelas;
  String nome;
  double peso;
  double preco;
  double precoPromocional;
  double precoUnitario;
  ProdutoSistemaModel produtoSistema;
  int quantidedeMinima;
  bool retirarLoja;
  bool soDinheiro;
  int totalEstoque;

  ProdutoModel   ({
    this.aceitaBoleto,
    this.aceitaCartaoExterno,
    this.altura,
    this.ativo,
    this.categoria,
    this.clientePrincipal,
    this.codigoEAN,
    this.codigoNCM,
    this.comprimento,
    this.criado,
    this.descricao,
    this.disponivelLimiteTimestamp,
    this.empresa,
    this.entregaGratis,
    this.familia,
    this.fornecedorPrincipal,
    this.fotoPrincipalUrl,
    this.id,
    this.largura,
    this.maximoParcelas,
    this.nome,
    this.peso,
    this.preco,
    this.precoPromocional,
    this.precoUnitario,
    this.produtoSistema,
    this.quantidedeMinima,
    this.retirarLoja,
    this.soDinheiro,
    this.totalEstoque,
  });

  factory ProdutoModel.fromJson(Map<String, dynamic> json) =>  ProdutoModel(
    aceitaBoleto: Extractor.extractBool(json['aceitaBoleto']),
    aceitaCartaoExterno: Extractor.extractBool(json['aceitaCartaoExterno']),
    altura: Extractor.extractDouble(json['altura']),
    ativo: Extractor.extractBool(json['ativo']),
    categoria: CategoriaProdutoModel.fromJson(json['categoria']),
    clientePrincipal: EmpresaModel.fromJson(json['clientePrincipal']),
    codigoEAN: Extractor.extractString(json['codigoEAN'], ''),
    codigoNCM: Extractor.extractString(json['codigoNCM'], ''),
    comprimento: Extractor.extractDouble(json['comprimento']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    disponivelLimiteTimestamp: Extractor.extractDate(json['disponivelLimiteTimestamp']),
    empresa: EmpresaModel.fromJson(json['empresa']),
    entregaGratis: Extractor.extractBool(json['entregaGratis']),
    familia: FamiliaProdutoModel.fromJson(json['familia']),
    fornecedorPrincipal: EmpresaModel.fromJson(json['fornecedorPrincipal']),
    fotoPrincipalUrl: Extractor.extractString(json['fotoPrincipalUrl'], ''),
    id: json['id'],
    largura: Extractor.extractDouble(json['largura']),
    maximoParcelas: Extractor.extractInt(json['maximoParcelas']),
    nome: Extractor.extractString(json['nome'], ''),
    peso: Extractor.extractDouble(json['peso']),
    preco: Extractor.extractDouble(json['preco']),
    precoPromocional: Extractor.extractDouble(json['precoPromocional']),
    precoUnitario: Extractor.extractDouble(json['precoUnitario']),
    produtoSistema: ProdutoSistemaModel.fromJson(json['produtoSistema']),
    quantidedeMinima: Extractor.extractInt(json['quantidedeMinima']),
    retirarLoja: Extractor.extractBool(json['retirarLoja']),
    soDinheiro: Extractor.extractBool(json['soDinheiro']),
    totalEstoque: Extractor.extractInt(json['totalEstoque']),
  );

  Map<String, dynamic> toMap() => { 
    'aceitaBoleto': aceitaBoleto,
    'aceitaCartaoExterno': aceitaCartaoExterno,
    'altura': altura,
    'ativo': ativo,
    'categoria': categoria.toMap(),
    'clientePrincipal': clientePrincipal.toMap(),
    'codigoEAN': codigoEAN,
    'codigoNCM': codigoNCM,
    'comprimento': comprimento,
    'criado': criado,
    'descricao': descricao,
    'disponivelLimiteTimestamp': disponivelLimiteTimestamp,
    'empresa': empresa.toMap(),
    'entregaGratis': entregaGratis,
    'familia': familia.toMap(),
    'fornecedorPrincipal': fornecedorPrincipal.toMap(),
    'fotoPrincipalUrl': fotoPrincipalUrl,
    'id': id,
    'largura': largura,
    'maximoParcelas': maximoParcelas,
    'nome': nome,
    'peso': peso,
    'preco': preco,
    'precoPromocional': precoPromocional,
    'precoUnitario': precoUnitario,
    'produtoSistema': produtoSistema.toMap(),
    'quantidedeMinima': quantidedeMinima,
    'retirarLoja': retirarLoja,
    'soDinheiro': soDinheiro,
    'totalEstoque': totalEstoque,
  };

  static List<ProdutoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ProdutoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ProdutoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}