class CentroCustoModel {
  bool ativo;
  DateTime criado;
  String descricao;
  EmpresaModel empresa;
  String iconeUrl;
  int id;
  String nome;
  bool sistema;

  CentroCustoModel   ({
    this.ativo,
    this.criado,
    this.descricao,
    this.empresa,
    this.iconeUrl,
    this.id,
    this.nome,
    this.sistema,
  });

  factory CentroCustoModel.fromJson(Map<String, dynamic> json) =>  CentroCustoModel(
    ativo: Extractor.extractBool(json['ativo']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    iconeUrl: Extractor.extractString(json['iconeUrl'], ''),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
    sistema: Extractor.extractBool(json['sistema']),
  );

  Map<String, dynamic> toMap() => { 
    'ativo': ativo,
    'criado': criado,
    'descricao': descricao,
    'empresa': empresa.toMap(),
    'iconeUrl': iconeUrl,
    'id': id,
    'nome': nome,
    'sistema': sistema,
  };

  static List<CentroCustoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => CentroCustoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CentroCustoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}