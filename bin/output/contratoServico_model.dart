class ContratoServicoModel {
  bool aceito;
  EmpresaModel cliente;
  DateTime criado;
  int diaFaturamento;
  EmpresaModel fornecedor;
  int id;
  NotaFiscalModel notaFiscal;
  String numeroContrato;
  int numeroParcelas;
  String observacoes;
  DateTime previsaoFaturamento;
  List<ServicoModel> servicos;
  FuncionarioModel vendedor;
  DateTime vigenciaFinal;
  DateTime vigenciaInicial;

  ContratoServicoModel   ({
    this.aceito,
    this.cliente,
    this.criado,
    this.diaFaturamento,
    this.fornecedor,
    this.id,
    this.notaFiscal,
    this.numeroContrato,
    this.numeroParcelas,
    this.observacoes,
    this.previsaoFaturamento,
    this.servicos,
    this.vendedor,
    this.vigenciaFinal,
    this.vigenciaInicial,
  });

  factory ContratoServicoModel.fromJson(Map<String, dynamic> json) =>  ContratoServicoModel(
    aceito: Extractor.extractBool(json['aceito']),
    cliente: EmpresaModel.fromJson(json['cliente']),
    criado: Extractor.extractDate(json['criado']),
    diaFaturamento: Extractor.extractInt(json['diaFaturamento']),
    fornecedor: EmpresaModel.fromJson(json['fornecedor']),
    id: json['id'],
    notaFiscal: NotaFiscalModel.fromJson(json['notaFiscal']),
    numeroContrato: Extractor.extractString(json['numeroContrato'], ''),
    numeroParcelas: Extractor.extractInt(json['numeroParcelas']),
    observacoes: Extractor.extractString(json['observacoes'], ''),
    previsaoFaturamento: Extractor.extractDate(json['previsaoFaturamento']),
    servicos: ServicoModel.listFromJson(json['servicos']),
    vendedor: FuncionarioModel.fromJson(json['vendedor']),
    vigenciaFinal: Extractor.extractDate(json['vigenciaFinal']),
    vigenciaInicial: Extractor.extractDate(json['vigenciaInicial']),
  );

  Map<String, dynamic> toMap() => { 
    'aceito': aceito,
    'cliente': cliente.toMap(),
    'criado': criado,
    'diaFaturamento': diaFaturamento,
    'fornecedor': fornecedor.toMap(),
    'id': id,
    'notaFiscal': notaFiscal.toMap(),
    'numeroContrato': numeroContrato,
    'numeroParcelas': numeroParcelas,
    'observacoes': observacoes,
    'previsaoFaturamento': previsaoFaturamento,
    'servicos': ContratoServicoModel.listToMap(servicos),
    'vendedor': vendedor.toMap(),
    'vigenciaFinal': vigenciaFinal,
    'vigenciaInicial': vigenciaInicial,
  };

  static List<ContratoServicoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ContratoServicoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ContratoServicoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}