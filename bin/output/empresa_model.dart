class EmpresaModel {
  String cnpj;
  DateTime criado;
  String email;
  List<EmpresaModel> fornecedores;
  DateTime fundacao;
  int id;
  List<ImpostoModel> impostos;
  String nome;
  String pessoaContato;
  String razaoSocial;
  String sigla;
  String telefone;

  EmpresaModel   ({
    this.cnpj,
    this.criado,
    this.email,
    this.fornecedores,
    this.fundacao,
    this.id,
    this.impostos,
    this.nome,
    this.pessoaContato,
    this.razaoSocial,
    this.sigla,
    this.telefone,
  });

  factory EmpresaModel.fromJson(Map<String, dynamic> json) =>  EmpresaModel(
    cnpj: Extractor.extractString(json['cnpj'], ''),
    criado: Extractor.extractDate(json['criado']),
    email: Extractor.extractString(json['email'], ''),
    fornecedores: EmpresaModel.listFromJson(json['fornecedores']),
    fundacao: Extractor.extractDate(json['fundacao']),
    id: json['id'],
    impostos: ImpostoModel.listFromJson(json['impostos']),
    nome: Extractor.extractString(json['nome'], ''),
    pessoaContato: Extractor.extractString(json['pessoaContato'], ''),
    razaoSocial: Extractor.extractString(json['razaoSocial'], ''),
    sigla: Extractor.extractString(json['sigla'], ''),
    telefone: Extractor.extractString(json['telefone'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'cnpj': cnpj,
    'criado': criado,
    'email': email,
    'fornecedores': EmpresaModel.listToMap(fornecedores),
    'fundacao': fundacao,
    'id': id,
    'impostos': EmpresaModel.listToMap(impostos),
    'nome': nome,
    'pessoaContato': pessoaContato,
    'razaoSocial': razaoSocial,
    'sigla': sigla,
    'telefone': telefone,
  };

  static List<EmpresaModel> listFromJson(List<dynamic> data) {
    return data.map((post) => EmpresaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<EmpresaModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}