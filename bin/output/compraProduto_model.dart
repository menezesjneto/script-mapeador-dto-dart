class CompraProdutoModel {
  CompraProdutosModel compraProdutos;
  EmpresaModel fornecedor;
  int id;
  String observacoes;
  ProdutoModel produto;
  double quantidade;
  double valorTotal;
  double valorUnitario;

  CompraProdutoModel   ({
    this.compraProdutos,
    this.fornecedor,
    this.id,
    this.observacoes,
    this.produto,
    this.quantidade,
    this.valorTotal,
    this.valorUnitario,
  });

  factory CompraProdutoModel.fromJson(Map<String, dynamic> json) =>  CompraProdutoModel(
    compraProdutos: CompraProdutosModel.fromJson(json['compraProdutos']),
    fornecedor: EmpresaModel.fromJson(json['fornecedor']),
    id: json['id'],
    observacoes: Extractor.extractString(json['observacoes'], ''),
    produto: ProdutoModel.fromJson(json['produto']),
    quantidade: Extractor.extractDouble(json['quantidade']),
    valorTotal: Extractor.extractDouble(json['valorTotal']),
    valorUnitario: Extractor.extractDouble(json['valorUnitario']),
  );

  Map<String, dynamic> toMap() => { 
    'compraProdutos': compraProdutos.toMap(),
    'fornecedor': fornecedor.toMap(),
    'id': id,
    'observacoes': observacoes,
    'produto': produto.toMap(),
    'quantidade': quantidade,
    'valorTotal': valorTotal,
    'valorUnitario': valorUnitario,
  };

  static List<CompraProdutoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => CompraProdutoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CompraProdutoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}