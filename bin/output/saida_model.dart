class SaidaModel {
  String arquivoNotaFiscalUrl;
  AtividadeModel atividade;
  CentroCustoModel categoria;
  CompraProdutosModel compraProdutos;
  CompraServicoModel compraServico;
  ContaBancariaModel conta;
  DateTime criado;
  double desconto;
  EmpresaModel destino;
  EmpresaModel empresa;
  int id;
  double impRetidoCOFINS;
  double impRetidoCSLL;
  double impRetidoINSS;
  double impRetidoIR;
  double impRetidoISS;
  double impRetidoPIS;
  InstanciaImpostoNaEmpresaModel instanciaImpostoNaEmpresa;
  double juros;
  String nome;
  NotaFiscalModel notaFiscal;
  String observacoes;
  DateTime previsaoPagamento;
  ProjetoModel projeto;
  double valor;
  DateTime vencimento;

  SaidaModel   ({
    this.arquivoNotaFiscalUrl,
    this.atividade,
    this.categoria,
    this.compraProdutos,
    this.compraServico,
    this.conta,
    this.criado,
    this.desconto,
    this.destino,
    this.empresa,
    this.id,
    this.impRetidoCOFINS,
    this.impRetidoCSLL,
    this.impRetidoINSS,
    this.impRetidoIR,
    this.impRetidoISS,
    this.impRetidoPIS,
    this.instanciaImpostoNaEmpresa,
    this.juros,
    this.nome,
    this.notaFiscal,
    this.observacoes,
    this.previsaoPagamento,
    this.projeto,
    this.valor,
    this.vencimento,
  });

  factory SaidaModel.fromJson(Map<String, dynamic> json) =>  SaidaModel(
    arquivoNotaFiscalUrl: Extractor.extractString(json['arquivoNotaFiscalUrl'], ''),
    atividade: AtividadeModel.fromJson(json['atividade']),
    categoria: CentroCustoModel.fromJson(json['categoria']),
    compraProdutos: CompraProdutosModel.fromJson(json['compraProdutos']),
    compraServico: CompraServicoModel.fromJson(json['compraServico']),
    conta: ContaBancariaModel.fromJson(json['conta']),
    criado: Extractor.extractDate(json['criado']),
    desconto: Extractor.extractDouble(json['desconto']),
    destino: EmpresaModel.fromJson(json['destino']),
    empresa: EmpresaModel.fromJson(json['empresa']),
    id: json['id'],
    impRetidoCOFINS: Extractor.extractDouble(json['impRetidoCOFINS']),
    impRetidoCSLL: Extractor.extractDouble(json['impRetidoCSLL']),
    impRetidoINSS: Extractor.extractDouble(json['impRetidoINSS']),
    impRetidoIR: Extractor.extractDouble(json['impRetidoIR']),
    impRetidoISS: Extractor.extractDouble(json['impRetidoISS']),
    impRetidoPIS: Extractor.extractDouble(json['impRetidoPIS']),
    instanciaImpostoNaEmpresa: InstanciaImpostoNaEmpresaModel.fromJson(json['instanciaImpostoNaEmpresa']),
    juros: Extractor.extractDouble(json['juros']),
    nome: Extractor.extractString(json['nome'], ''),
    notaFiscal: NotaFiscalModel.fromJson(json['notaFiscal']),
    observacoes: Extractor.extractString(json['observacoes'], ''),
    previsaoPagamento: Extractor.extractDate(json['previsaoPagamento']),
    projeto: ProjetoModel.fromJson(json['projeto']),
    valor: Extractor.extractDouble(json['valor']),
    vencimento: Extractor.extractDate(json['vencimento']),
  );

  Map<String, dynamic> toMap() => { 
    'arquivoNotaFiscalUrl': arquivoNotaFiscalUrl,
    'atividade': atividade.toMap(),
    'categoria': categoria.toMap(),
    'compraProdutos': compraProdutos.toMap(),
    'compraServico': compraServico.toMap(),
    'conta': conta.toMap(),
    'criado': criado,
    'desconto': desconto,
    'destino': destino.toMap(),
    'empresa': empresa.toMap(),
    'id': id,
    'impRetidoCOFINS': impRetidoCOFINS,
    'impRetidoCSLL': impRetidoCSLL,
    'impRetidoINSS': impRetidoINSS,
    'impRetidoIR': impRetidoIR,
    'impRetidoISS': impRetidoISS,
    'impRetidoPIS': impRetidoPIS,
    'instanciaImpostoNaEmpresa': instanciaImpostoNaEmpresa.toMap(),
    'juros': juros,
    'nome': nome,
    'notaFiscal': notaFiscal.toMap(),
    'observacoes': observacoes,
    'previsaoPagamento': previsaoPagamento,
    'projeto': projeto.toMap(),
    'valor': valor,
    'vencimento': vencimento,
  };

  static List<SaidaModel> listFromJson(List<dynamic> data) {
    return data.map((post) => SaidaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<SaidaModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}