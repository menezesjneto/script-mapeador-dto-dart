class CategoriaProdutoModel {
  bool ativo;
  String codigo;
  DateTime criadaTimestamp;
  String descricao;
  int id;
  String nome;
  String urlImagem;

  CategoriaProdutoModel   ({
    this.ativo,
    this.codigo,
    this.criadaTimestamp,
    this.descricao,
    this.id,
    this.nome,
    this.urlImagem,
  });

  factory CategoriaProdutoModel.fromJson(Map<String, dynamic> json) =>  CategoriaProdutoModel(
    ativo: Extractor.extractBool(json['ativo']),
    codigo: Extractor.extractString(json['codigo'], ''),
    criadaTimestamp: Extractor.extractDate(json['criadaTimestamp']),
    descricao: Extractor.extractString(json['descricao'], ''),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
    urlImagem: Extractor.extractString(json['urlImagem'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'ativo': ativo,
    'codigo': codigo,
    'criadaTimestamp': criadaTimestamp,
    'descricao': descricao,
    'id': id,
    'nome': nome,
    'urlImagem': urlImagem,
  };

  static List<CategoriaProdutoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => CategoriaProdutoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CategoriaProdutoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}