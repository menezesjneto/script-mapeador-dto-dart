class PessoaModel {
  bool ativo;
  String celular;
  String comoConhecido;
  String cpf;
  DateTime criado;
  DateTime dataNascimento;
  String email;
  EmpresaModel empresa;
  String fixo;
  String foto;
  int id;
  String naturalidade;
  String nome;
  String tokenPush;
  DateTime ultimoLogin;
  UserModel user;

  PessoaModel   ({
    this.ativo,
    this.celular,
    this.comoConhecido,
    this.cpf,
    this.criado,
    this.dataNascimento,
    this.email,
    this.empresa,
    this.fixo,
    this.foto,
    this.id,
    this.naturalidade,
    this.nome,
    this.tokenPush,
    this.ultimoLogin,
    this.user,
  });

  factory PessoaModel.fromJson(Map<String, dynamic> json) =>  PessoaModel(
    ativo: Extractor.extractBool(json['ativo']),
    celular: Extractor.extractString(json['celular'], ''),
    comoConhecido: Extractor.extractString(json['comoConhecido'], ''),
    cpf: Extractor.extractString(json['cpf'], ''),
    criado: Extractor.extractDate(json['criado']),
    dataNascimento: Extractor.extractDate(json['dataNascimento']),
    email: Extractor.extractString(json['email'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    fixo: Extractor.extractString(json['fixo'], ''),
    foto: Extractor.extractString(json['foto'], ''),
    id: json['id'],
    naturalidade: Extractor.extractString(json['naturalidade'], ''),
    nome: Extractor.extractString(json['nome'], ''),
    tokenPush: Extractor.extractString(json['tokenPush'], ''),
    ultimoLogin: Extractor.extractDate(json['ultimoLogin']),
    user: UserModel.fromJson(json['user']),
  );

  Map<String, dynamic> toMap() => { 
    'ativo': ativo,
    'celular': celular,
    'comoConhecido': comoConhecido,
    'cpf': cpf,
    'criado': criado,
    'dataNascimento': dataNascimento,
    'email': email,
    'empresa': empresa.toMap(),
    'fixo': fixo,
    'foto': foto,
    'id': id,
    'naturalidade': naturalidade,
    'nome': nome,
    'tokenPush': tokenPush,
    'ultimoLogin': ultimoLogin,
    'user': user.toMap(),
  };

  static List<PessoaModel> listFromJson(List<dynamic> data) {
    return data.map((post) => PessoaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<PessoaModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}