class ArquivoAnexoModel {
  AtividadeModel atividade;
  CertidaoModel certidao;
  CertificadoModel certificado;
  ConteudoModel conteudo;
  CooperadoModel cooperadoAnexo;
  DateTime criado;
  String descricao;
  EmpresaModel empresa;
  EmpresaModel empresaAnexo;
  EntradaModel entrada;
  String extensao;
  String filePath;
  FuncionarioModel funcionarioAnexo;
  int id;
  ImpostoModel imposto;
  InstanciaImpostoNaEmpresaModel instanciaImposto;
  LicencaModel licenca;
  String nome;
  PatrimonioModel patrimonio;
  SaidaModel saida;
  TemplateModel template;

  ArquivoAnexoModel   ({
    this.atividade,
    this.certidao,
    this.certificado,
    this.conteudo,
    this.cooperadoAnexo,
    this.criado,
    this.descricao,
    this.empresa,
    this.empresaAnexo,
    this.entrada,
    this.extensao,
    this.filePath,
    this.funcionarioAnexo,
    this.id,
    this.imposto,
    this.instanciaImposto,
    this.licenca,
    this.nome,
    this.patrimonio,
    this.saida,
    this.template,
  });

  factory ArquivoAnexoModel.fromJson(Map<String, dynamic> json) =>  ArquivoAnexoModel(
    atividade: AtividadeModel.fromJson(json['atividade']),
    certidao: CertidaoModel.fromJson(json['certidao']),
    certificado: CertificadoModel.fromJson(json['certificado']),
    conteudo: ConteudoModel.fromJson(json['conteudo']),
    cooperadoAnexo: CooperadoModel.fromJson(json['cooperadoAnexo']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    empresaAnexo: EmpresaModel.fromJson(json['empresaAnexo']),
    entrada: EntradaModel.fromJson(json['entrada']),
    extensao: Extractor.extractString(json['extensao'], ''),
    filePath: Extractor.extractString(json['filePath'], ''),
    funcionarioAnexo: FuncionarioModel.fromJson(json['funcionarioAnexo']),
    id: json['id'],
    imposto: ImpostoModel.fromJson(json['imposto']),
    instanciaImposto: InstanciaImpostoNaEmpresaModel.fromJson(json['instanciaImposto']),
    licenca: LicencaModel.fromJson(json['licenca']),
    nome: Extractor.extractString(json['nome'], ''),
    patrimonio: PatrimonioModel.fromJson(json['patrimonio']),
    saida: SaidaModel.fromJson(json['saida']),
    template: TemplateModel.fromJson(json['template']),
  );

  Map<String, dynamic> toMap() => { 
    'atividade': atividade.toMap(),
    'certidao': certidao.toMap(),
    'certificado': certificado.toMap(),
    'conteudo': conteudo.toMap(),
    'cooperadoAnexo': cooperadoAnexo.toMap(),
    'criado': criado,
    'descricao': descricao,
    'empresa': empresa.toMap(),
    'empresaAnexo': empresaAnexo.toMap(),
    'entrada': entrada.toMap(),
    'extensao': extensao,
    'filePath': filePath,
    'funcionarioAnexo': funcionarioAnexo.toMap(),
    'id': id,
    'imposto': imposto.toMap(),
    'instanciaImposto': instanciaImposto.toMap(),
    'licenca': licenca.toMap(),
    'nome': nome,
    'patrimonio': patrimonio.toMap(),
    'saida': saida.toMap(),
    'template': template.toMap(),
  };

  static List<ArquivoAnexoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ArquivoAnexoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ArquivoAnexoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}