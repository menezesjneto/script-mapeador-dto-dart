class ContratoModel {
  String arquivo;
  DateTime criado;
  String descricao;
  EmpresaModel empresa;
  int id;
  DateTime inicio;
  String numero;
  DateTime validade;

  ContratoModel   ({
    this.arquivo,
    this.criado,
    this.descricao,
    this.empresa,
    this.id,
    this.inicio,
    this.numero,
    this.validade,
  });

  factory ContratoModel.fromJson(Map<String, dynamic> json) =>  ContratoModel(
    arquivo: Extractor.extractString(json['arquivo'], ''),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    id: json['id'],
    inicio: Extractor.extractDate(json['inicio']),
    numero: Extractor.extractString(json['numero'], ''),
    validade: Extractor.extractDate(json['validade']),
  );

  Map<String, dynamic> toMap() => { 
    'arquivo': arquivo,
    'criado': criado,
    'descricao': descricao,
    'empresa': empresa.toMap(),
    'id': id,
    'inicio': inicio,
    'numero': numero,
    'validade': validade,
  };

  static List<ContratoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ContratoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ContratoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}