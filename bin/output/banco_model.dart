class BancoModel {
  int codigo;
  String contato;
  int id;
  String nome;
  String site;
  String urlImagem;

  BancoModel   ({
    this.codigo,
    this.contato,
    this.id,
    this.nome,
    this.site,
    this.urlImagem,
  });

  factory BancoModel.fromJson(Map<String, dynamic> json) =>  BancoModel(
    codigo: Extractor.extractInt(json['codigo']),
    contato: Extractor.extractString(json['contato'], ''),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
    site: Extractor.extractString(json['site'], ''),
    urlImagem: Extractor.extractString(json['urlImagem'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'codigo': codigo,
    'contato': contato,
    'id': id,
    'nome': nome,
    'site': site,
    'urlImagem': urlImagem,
  };

  static List<BancoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => BancoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<BancoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}