class CompraProdutosModel {
  String arquivoRecibo;
  bool confirmada;
  ContratoModel contrato;
  DateTime dataCompra;
  DateTime dataRecebimento;
  EmpresaModel empresa;
  EmpresaModel fornecedor;
  int id;
  String nome;
  NotaFiscalModel notaFiscal;
  String observacoes;
  int parcelas;
  ProjetoModel projeto;
  double valorTotal;

  CompraProdutosModel   ({
    this.arquivoRecibo,
    this.confirmada,
    this.contrato,
    this.dataCompra,
    this.dataRecebimento,
    this.empresa,
    this.fornecedor,
    this.id,
    this.nome,
    this.notaFiscal,
    this.observacoes,
    this.parcelas,
    this.projeto,
    this.valorTotal,
  });

  factory CompraProdutosModel.fromJson(Map<String, dynamic> json) =>  CompraProdutosModel(
    arquivoRecibo: Extractor.extractString(json['arquivoRecibo'], ''),
    confirmada: Extractor.extractBool(json['confirmada']),
    contrato: ContratoModel.fromJson(json['contrato']),
    dataCompra: Extractor.extractDate(json['dataCompra']),
    dataRecebimento: Extractor.extractDate(json['dataRecebimento']),
    empresa: EmpresaModel.fromJson(json['empresa']),
    fornecedor: EmpresaModel.fromJson(json['fornecedor']),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
    notaFiscal: NotaFiscalModel.fromJson(json['notaFiscal']),
    observacoes: Extractor.extractString(json['observacoes'], ''),
    parcelas: Extractor.extractInt(json['parcelas']),
    projeto: ProjetoModel.fromJson(json['projeto']),
    valorTotal: Extractor.extractDouble(json['valorTotal']),
  );

  Map<String, dynamic> toMap() => { 
    'arquivoRecibo': arquivoRecibo,
    'confirmada': confirmada,
    'contrato': contrato.toMap(),
    'dataCompra': dataCompra,
    'dataRecebimento': dataRecebimento,
    'empresa': empresa.toMap(),
    'fornecedor': fornecedor.toMap(),
    'id': id,
    'nome': nome,
    'notaFiscal': notaFiscal.toMap(),
    'observacoes': observacoes,
    'parcelas': parcelas,
    'projeto': projeto.toMap(),
    'valorTotal': valorTotal,
  };

  static List<CompraProdutosModel> listFromJson(List<dynamic> data) {
    return data.map((post) => CompraProdutosModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CompraProdutosModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}