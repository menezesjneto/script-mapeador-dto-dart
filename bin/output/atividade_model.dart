class AtividadeModel {
  List<PessoaModel> alocados;
  DateTime criado;
  List<AtividadeModel> dependentes;
  String descricao;
  DateTime execucaoFim;
  double execucaoHoras;
  DateTime execucaoInicio;
  int id;
  String nome;
  DateTime previsaoFim;
  double previsaoHoras;
  DateTime previsaoInicio;
  ProjetoModel projeto;

  AtividadeModel   ({
    this.alocados,
    this.criado,
    this.dependentes,
    this.descricao,
    this.execucaoFim,
    this.execucaoHoras,
    this.execucaoInicio,
    this.id,
    this.nome,
    this.previsaoFim,
    this.previsaoHoras,
    this.previsaoInicio,
    this.projeto,
  });

  factory AtividadeModel.fromJson(Map<String, dynamic> json) =>  AtividadeModel(
    alocados: PessoaModel.listFromJson(json['alocados']),
    criado: Extractor.extractDate(json['criado']),
    dependentes: AtividadeModel.listFromJson(json['dependentes']),
    descricao: Extractor.extractString(json['descricao'], ''),
    execucaoFim: Extractor.extractDate(json['execucaoFim']),
    execucaoHoras: Extractor.extractDouble(json['execucaoHoras']),
    execucaoInicio: Extractor.extractDate(json['execucaoInicio']),
    id: json['id'],
    nome: Extractor.extractString(json['nome'], ''),
    previsaoFim: Extractor.extractDate(json['previsaoFim']),
    previsaoHoras: Extractor.extractDouble(json['previsaoHoras']),
    previsaoInicio: Extractor.extractDate(json['previsaoInicio']),
    projeto: ProjetoModel.fromJson(json['projeto']),
  );

  Map<String, dynamic> toMap() => { 
    'alocados': AtividadeModel.listToMap(alocados),
    'criado': criado,
    'dependentes': AtividadeModel.listToMap(dependentes),
    'descricao': descricao,
    'execucaoFim': execucaoFim,
    'execucaoHoras': execucaoHoras,
    'execucaoInicio': execucaoInicio,
    'id': id,
    'nome': nome,
    'previsaoFim': previsaoFim,
    'previsaoHoras': previsaoHoras,
    'previsaoInicio': previsaoInicio,
    'projeto': projeto.toMap(),
  };

  static List<AtividadeModel> listFromJson(List<dynamic> data) {
    return data.map((post) => AtividadeModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<AtividadeModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}