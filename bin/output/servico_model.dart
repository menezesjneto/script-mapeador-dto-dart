class ServicoModel {
  bool aceitaBoleto;
  bool aceitaCartaoExterno;
  double aliquotaCOFINS;
  double aliquotaCSLL;
  double aliquotaINSS;
  double aliquotaIR;
  double aliquotaISS;
  double aliquotaPIS;
  bool ativo;
  String codigoCNAE;
  String codigoLC116;
  String codigoNBS;
  DateTime criado;
  String descricao;
  EmpresaModel empresa;
  String fotoPrincipalUrl;
  int id;
  int maximoParcelas;
  String nome;
  double precoCusto;
  double precoPromocional;
  double precoVenda;
  int quantidedeMinima;
  bool soDinheiro;
  String tributacao;

  ServicoModel   ({
    this.aceitaBoleto,
    this.aceitaCartaoExterno,
    this.aliquotaCOFINS,
    this.aliquotaCSLL,
    this.aliquotaINSS,
    this.aliquotaIR,
    this.aliquotaISS,
    this.aliquotaPIS,
    this.ativo,
    this.codigoCNAE,
    this.codigoLC116,
    this.codigoNBS,
    this.criado,
    this.descricao,
    this.empresa,
    this.fotoPrincipalUrl,
    this.id,
    this.maximoParcelas,
    this.nome,
    this.precoCusto,
    this.precoPromocional,
    this.precoVenda,
    this.quantidedeMinima,
    this.soDinheiro,
    this.tributacao,
  });

  factory ServicoModel.fromJson(Map<String, dynamic> json) =>  ServicoModel(
    aceitaBoleto: Extractor.extractBool(json['aceitaBoleto']),
    aceitaCartaoExterno: Extractor.extractBool(json['aceitaCartaoExterno']),
    aliquotaCOFINS: Extractor.extractDouble(json['aliquotaCOFINS']),
    aliquotaCSLL: Extractor.extractDouble(json['aliquotaCSLL']),
    aliquotaINSS: Extractor.extractDouble(json['aliquotaINSS']),
    aliquotaIR: Extractor.extractDouble(json['aliquotaIR']),
    aliquotaISS: Extractor.extractDouble(json['aliquotaISS']),
    aliquotaPIS: Extractor.extractDouble(json['aliquotaPIS']),
    ativo: Extractor.extractBool(json['ativo']),
    codigoCNAE: Extractor.extractString(json['codigoCNAE'], ''),
    codigoLC116: Extractor.extractString(json['codigoLC116'], ''),
    codigoNBS: Extractor.extractString(json['codigoNBS'], ''),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    empresa: EmpresaModel.fromJson(json['empresa']),
    fotoPrincipalUrl: Extractor.extractString(json['fotoPrincipalUrl'], ''),
    id: json['id'],
    maximoParcelas: Extractor.extractInt(json['maximoParcelas']),
    nome: Extractor.extractString(json['nome'], ''),
    precoCusto: Extractor.extractDouble(json['precoCusto']),
    precoPromocional: Extractor.extractDouble(json['precoPromocional']),
    precoVenda: Extractor.extractDouble(json['precoVenda']),
    quantidedeMinima: Extractor.extractInt(json['quantidedeMinima']),
    soDinheiro: Extractor.extractBool(json['soDinheiro']),
    tributacao: Extractor.extractString(json['tributacao'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'aceitaBoleto': aceitaBoleto,
    'aceitaCartaoExterno': aceitaCartaoExterno,
    'aliquotaCOFINS': aliquotaCOFINS,
    'aliquotaCSLL': aliquotaCSLL,
    'aliquotaINSS': aliquotaINSS,
    'aliquotaIR': aliquotaIR,
    'aliquotaISS': aliquotaISS,
    'aliquotaPIS': aliquotaPIS,
    'ativo': ativo,
    'codigoCNAE': codigoCNAE,
    'codigoLC116': codigoLC116,
    'codigoNBS': codigoNBS,
    'criado': criado,
    'descricao': descricao,
    'empresa': empresa.toMap(),
    'fotoPrincipalUrl': fotoPrincipalUrl,
    'id': id,
    'maximoParcelas': maximoParcelas,
    'nome': nome,
    'precoCusto': precoCusto,
    'precoPromocional': precoPromocional,
    'precoVenda': precoVenda,
    'quantidedeMinima': quantidedeMinima,
    'soDinheiro': soDinheiro,
    'tributacao': tributacao,
  };

  static List<ServicoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => ServicoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ServicoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}