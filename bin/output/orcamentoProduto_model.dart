class OrcamentoProdutoModel {
  DateTime data;
  int id;
  String nomeProduto;
  OrcamentoModel orcamento;
  ProdutoModel produto;
  double quantidade;
  DateTime validade;
  double valor;

  OrcamentoProdutoModel   ({
    this.data,
    this.id,
    this.nomeProduto,
    this.orcamento,
    this.produto,
    this.quantidade,
    this.validade,
    this.valor,
  });

  factory OrcamentoProdutoModel.fromJson(Map<String, dynamic> json) =>  OrcamentoProdutoModel(
    data: Extractor.extractDate(json['data']),
    id: json['id'],
    nomeProduto: Extractor.extractString(json['nomeProduto'], ''),
    orcamento: OrcamentoModel.fromJson(json['orcamento']),
    produto: ProdutoModel.fromJson(json['produto']),
    quantidade: Extractor.extractDouble(json['quantidade']),
    validade: Extractor.extractDate(json['validade']),
    valor: Extractor.extractDouble(json['valor']),
  );

  Map<String, dynamic> toMap() => { 
    'data': data,
    'id': id,
    'nomeProduto': nomeProduto,
    'orcamento': orcamento.toMap(),
    'produto': produto.toMap(),
    'quantidade': quantidade,
    'validade': validade,
    'valor': valor,
  };

  static List<OrcamentoProdutoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => OrcamentoProdutoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<OrcamentoProdutoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}