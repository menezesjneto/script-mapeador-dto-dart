class CategoriaEmpresaModel {
  bool ativo;
  DateTime criado;
  List<EmpresaModel> empresas;
  int id;
  String imagemUrl;
  String nome;
  List<TemplateModel> templatesDisponiveis;

  CategoriaEmpresaModel   ({
    this.ativo,
    this.criado,
    this.empresas,
    this.id,
    this.imagemUrl,
    this.nome,
    this.templatesDisponiveis,
  });

  factory CategoriaEmpresaModel.fromJson(Map<String, dynamic> json) =>  CategoriaEmpresaModel(
    ativo: Extractor.extractBool(json['ativo']),
    criado: Extractor.extractDate(json['criado']),
    empresas: EmpresaModel.listFromJson(json['empresas']),
    id: json['id'],
    imagemUrl: Extractor.extractString(json['imagemUrl'], ''),
    nome: Extractor.extractString(json['nome'], ''),
    templatesDisponiveis: TemplateModel.listFromJson(json['templatesDisponiveis']),
  );

  Map<String, dynamic> toMap() => { 
    'ativo': ativo,
    'criado': criado,
    'empresas': CategoriaEmpresaModel.listToMap(empresas),
    'id': id,
    'imagemUrl': imagemUrl,
    'nome': nome,
    'templatesDisponiveis': CategoriaEmpresaModel.listToMap(templatesDisponiveis),
  };

  static List<CategoriaEmpresaModel> listFromJson(List<dynamic> data) {
    return data.map((post) => CategoriaEmpresaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CategoriaEmpresaModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}