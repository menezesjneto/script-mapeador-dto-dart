class EntradaModel {
  String arquivoNotaFiscalUrl;
  AtividadeModel atividade;
  CentroCustoModel categoria;
  ContaBancariaModel conta;
  ContratoServicoModel contrato;
  DateTime criado;
  double desconto;
  EmpresaModel empresa;
  int id;
  double impRetidoCOFINS;
  double impRetidoCSLL;
  double impRetidoINSS;
  double impRetidoIR;
  double impRetidoISS;
  double impRetidoPIS;
  double juros;
  String nome;
  NotaFiscalModel notaFiscal;
  String observacoes;
  OrcamentoModel orcamento;
  EmpresaModel origem;
  DateTime previsaoRecebimento;
  ProjetoModel projeto;
  double valor;
  DateTime vencimento;

  EntradaModel   ({
    this.arquivoNotaFiscalUrl,
    this.atividade,
    this.categoria,
    this.conta,
    this.contrato,
    this.criado,
    this.desconto,
    this.empresa,
    this.id,
    this.impRetidoCOFINS,
    this.impRetidoCSLL,
    this.impRetidoINSS,
    this.impRetidoIR,
    this.impRetidoISS,
    this.impRetidoPIS,
    this.juros,
    this.nome,
    this.notaFiscal,
    this.observacoes,
    this.orcamento,
    this.origem,
    this.previsaoRecebimento,
    this.projeto,
    this.valor,
    this.vencimento,
  });

  factory EntradaModel.fromJson(Map<String, dynamic> json) =>  EntradaModel(
    arquivoNotaFiscalUrl: Extractor.extractString(json['arquivoNotaFiscalUrl'], ''),
    atividade: AtividadeModel.fromJson(json['atividade']),
    categoria: CentroCustoModel.fromJson(json['categoria']),
    conta: ContaBancariaModel.fromJson(json['conta']),
    contrato: ContratoServicoModel.fromJson(json['contrato']),
    criado: Extractor.extractDate(json['criado']),
    desconto: Extractor.extractDouble(json['desconto']),
    empresa: EmpresaModel.fromJson(json['empresa']),
    id: json['id'],
    impRetidoCOFINS: Extractor.extractDouble(json['impRetidoCOFINS']),
    impRetidoCSLL: Extractor.extractDouble(json['impRetidoCSLL']),
    impRetidoINSS: Extractor.extractDouble(json['impRetidoINSS']),
    impRetidoIR: Extractor.extractDouble(json['impRetidoIR']),
    impRetidoISS: Extractor.extractDouble(json['impRetidoISS']),
    impRetidoPIS: Extractor.extractDouble(json['impRetidoPIS']),
    juros: Extractor.extractDouble(json['juros']),
    nome: Extractor.extractString(json['nome'], ''),
    notaFiscal: NotaFiscalModel.fromJson(json['notaFiscal']),
    observacoes: Extractor.extractString(json['observacoes'], ''),
    orcamento: OrcamentoModel.fromJson(json['orcamento']),
    origem: EmpresaModel.fromJson(json['origem']),
    previsaoRecebimento: Extractor.extractDate(json['previsaoRecebimento']),
    projeto: ProjetoModel.fromJson(json['projeto']),
    valor: Extractor.extractDouble(json['valor']),
    vencimento: Extractor.extractDate(json['vencimento']),
  );

  Map<String, dynamic> toMap() => { 
    'arquivoNotaFiscalUrl': arquivoNotaFiscalUrl,
    'atividade': atividade.toMap(),
    'categoria': categoria.toMap(),
    'conta': conta.toMap(),
    'contrato': contrato.toMap(),
    'criado': criado,
    'desconto': desconto,
    'empresa': empresa.toMap(),
    'id': id,
    'impRetidoCOFINS': impRetidoCOFINS,
    'impRetidoCSLL': impRetidoCSLL,
    'impRetidoINSS': impRetidoINSS,
    'impRetidoIR': impRetidoIR,
    'impRetidoISS': impRetidoISS,
    'impRetidoPIS': impRetidoPIS,
    'juros': juros,
    'nome': nome,
    'notaFiscal': notaFiscal.toMap(),
    'observacoes': observacoes,
    'orcamento': orcamento.toMap(),
    'origem': origem.toMap(),
    'previsaoRecebimento': previsaoRecebimento,
    'projeto': projeto.toMap(),
    'valor': valor,
    'vencimento': vencimento,
  };

  static List<EntradaModel> listFromJson(List<dynamic> data) {
    return data.map((post) => EntradaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<EntradaModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}