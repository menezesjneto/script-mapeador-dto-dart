class OrcamentoModel {
  bool aceito;
  AtividadeModel atividade;
  DateTime criado;
  String descricao;
  String formaPagamento;
  EmpresaModel fornecedor;
  int id;
  String informacoesComplementares;
  NotaFiscalModel notaFiscal;
  String previsaoEntrega;
  PessoaModel responsavel;
  EmpresaModel solicitante;
  String titulo;
  DateTime validade;

  OrcamentoModel   ({
    this.aceito,
    this.atividade,
    this.criado,
    this.descricao,
    this.formaPagamento,
    this.fornecedor,
    this.id,
    this.informacoesComplementares,
    this.notaFiscal,
    this.previsaoEntrega,
    this.responsavel,
    this.solicitante,
    this.titulo,
    this.validade,
  });

  factory OrcamentoModel.fromJson(Map<String, dynamic> json) =>  OrcamentoModel(
    aceito: Extractor.extractBool(json['aceito']),
    atividade: AtividadeModel.fromJson(json['atividade']),
    criado: Extractor.extractDate(json['criado']),
    descricao: Extractor.extractString(json['descricao'], ''),
    formaPagamento: Extractor.extractString(json['formaPagamento'], ''),
    fornecedor: EmpresaModel.fromJson(json['fornecedor']),
    id: json['id'],
    informacoesComplementares: Extractor.extractString(json['informacoesComplementares'], ''),
    notaFiscal: NotaFiscalModel.fromJson(json['notaFiscal']),
    previsaoEntrega: Extractor.extractString(json['previsaoEntrega'], ''),
    responsavel: PessoaModel.fromJson(json['responsavel']),
    solicitante: EmpresaModel.fromJson(json['solicitante']),
    titulo: Extractor.extractString(json['titulo'], ''),
    validade: Extractor.extractDate(json['validade']),
  );

  Map<String, dynamic> toMap() => { 
    'aceito': aceito,
    'atividade': atividade.toMap(),
    'criado': criado,
    'descricao': descricao,
    'formaPagamento': formaPagamento,
    'fornecedor': fornecedor.toMap(),
    'id': id,
    'informacoesComplementares': informacoesComplementares,
    'notaFiscal': notaFiscal.toMap(),
    'previsaoEntrega': previsaoEntrega,
    'responsavel': responsavel.toMap(),
    'solicitante': solicitante.toMap(),
    'titulo': titulo,
    'validade': validade,
  };

  static List<OrcamentoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => OrcamentoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<OrcamentoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}