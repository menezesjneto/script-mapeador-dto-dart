class CooperadoModel {
  bool admin;
  bool ativo;
  DateTime criado;
  EmpresaModel empresaAdministrada;
  List<EmpresaModel> empresas;
  int id;
  PessoaModel pessoa;
  List<ProdutoModel> produtos;
  List<ServicoModel> servicos;

  CooperadoModel   ({
    this.admin,
    this.ativo,
    this.criado,
    this.empresaAdministrada,
    this.empresas,
    this.id,
    this.pessoa,
    this.produtos,
    this.servicos,
  });

  factory CooperadoModel.fromJson(Map<String, dynamic> json) =>  CooperadoModel(
    admin: Extractor.extractBool(json['admin']),
    ativo: Extractor.extractBool(json['ativo']),
    criado: Extractor.extractDate(json['criado']),
    empresaAdministrada: EmpresaModel.fromJson(json['empresaAdministrada']),
    empresas: EmpresaModel.listFromJson(json['empresas']),
    id: json['id'],
    pessoa: PessoaModel.fromJson(json['pessoa']),
    produtos: ProdutoModel.listFromJson(json['produtos']),
    servicos: ServicoModel.listFromJson(json['servicos']),
  );

  Map<String, dynamic> toMap() => { 
    'admin': admin,
    'ativo': ativo,
    'criado': criado,
    'empresaAdministrada': empresaAdministrada.toMap(),
    'empresas': CooperadoModel.listToMap(empresas),
    'id': id,
    'pessoa': pessoa.toMap(),
    'produtos': CooperadoModel.listToMap(produtos),
    'servicos': CooperadoModel.listToMap(servicos),
  };

  static List<CooperadoModel> listFromJson(List<dynamic> data) {
    return data.map((post) => CooperadoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CooperadoModel> data) {
    return data.map((post) => post.toMap()).toList();
  }

}