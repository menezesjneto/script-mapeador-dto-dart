import 'dart:io';
import 'package:string_validator/string_validator.dart';
import 'models/field.dart';
import 'models/group.dart';
import 'utils/string_extension.dart';

void main(List<String> arguments) async {
  // Diretório atual
  Directory current = Directory.current;

  // Ler o arquivo coms os DTOS 
  File file = File('${current.path}/bin/input/dtos.txt');

  // Ler por linhas 
  List<String> fileLines = await file.readAsLines(); 

  //Remove as linhas em branco
  fileLines.removeWhere((element) => element == '');

  // Cria o nome de cada arquivo da classe, por exemplo: 
  //o nome ArquivoAnexoDTO  gera o arquivo arquivoAnexo_model.dart e a classe ArquivoAnexoModel
  //IMPORTANTE: O script identifica um modelo na linha que contem "DTO - ",
  //por exemplo: ArquivoAnexoDTO - ArquivoAnexoDTO
  List<String> classNamesLines = [];
  List<Group> groups = [];

  for (var item in (fileLines.where((element) => element.contains('DTO - '))).toList()) {
    Group group = new Group();

    classNamesLines.add(item);

    group.nameFileClass = item.split(" - ")[0].replaceAll("DTO", "").capitalizeLower() + '_model.dart';
    
    group.nameClass = item.split(" - ")[0].replaceAll("DTO", "") + 'Model';

    groups.add(group);

  }

  //Identifica a linha de início e fim da classe, Contando com a primeira linha
  //do nome da Classe
  for (var i = 0; i < classNamesLines.length; i++) {
    groups[i].startLine = fileLines.indexOf(classNamesLines[i]);
  }
  
  for (var i = 0; i < groups.length; i++) {
    if((i+1) != groups.length){
      groups[i].endLine = groups[i+1].startLine - 1;
    }else{
      groups[i].endLine = fileLines.length - 1;
    }
  }

  //Mapeamento do nome dos campos e tipos

  for (var item in groups) {
    item.fields = [];
    //u = item.startLine + 1 por que a primeira linha é o nome da Classe
    Field field = new Field(
      fieldsEnum: []
    );
    int aux = 1;
    bool controlEnum = false;
    for (var u = item.startLine + 1; u <= item.endLine; u++) {
      controlEnum = false;
      if(aux == 1){
        field.nameField = fileLines[u];
        aux++;
      } else{
        field.type = fileLines[u];
        item.fields.add(field);
        aux = 1;
        field = Field(
          fieldsEnum: []
        );
      }

      if(controlEnum) break;
    }
    
    
  }

  for (var item in groups) {
    for (var f in item.fields) {
      f.nameField = f.nameField.replaceAll(' (optional)', '');
      f.type = f.type.replaceAll(' (optional)', '');
      f.type = f.type.replaceAll('DTO', '');
      f.type = f.type.replaceAll('array[', 'List<').replaceAll(']', '>');
      switch (f.type) {
        case 'Long format: int64':
          f.type = 'int';
          if(f.nameField == 'id'){
            f.extractor = "json['${f.nameField}']";
          }else{
            f.extractor = "Extractor.extractInt(json['${f.nameField}'])";
          }
          break;
        case 'Integer format: int32':
          f.type = 'int';
          if(f.nameField == 'id'){
            f.extractor = "json['${f.nameField}']";
          }else{
            f.extractor = "Extractor.extractInt(json['${f.nameField}'])";
          }
          break;
        case 'Double format: double':
          f.type = 'double';
          f.extractor = "Extractor.extractDouble(json['${f.nameField}'])";
          break;
        case 'Date format: date-time':
          f.type = 'DateTime';
          f.extractor = "Extractor.extractDate(json['${f.nameField}'])";
          break;
        case 'date format: date':
          f.type = 'DateTime';
          f.extractor = "Extractor.extractDate(json['${f.nameField}'])";
          break;
        case 'String':
          f.type = 'String';
          if(f.nameField == 'id'){
            f.extractor = "json['${f.nameField}']";
          }else{
            f.extractor = "Extractor.extractString(json['${f.nameField}'], '')";
          }
          break;
        case 'Boolean':
          f.type = 'bool';
          f.extractor = "Extractor.extractBool(json['${f.nameField}'])";
          break;
        case 'Enum':
          f.type = 'Enum' + f.nameField.capitalize();
          f.extractor = f.nameField + 'StringToEnum(' + "json['${f.nameField}'])";
          break;
        default:
          if(f.type.contains('List')){
            f.type = f.type.replaceAll('List<', '').replaceAll('>', '') + 'Model';
            f.type = 'List<' + f.type  + '>';
            var typ = f.type.replaceAll('List<', '').replaceAll('>', '');
            f.extractor = typ + '.listFromJson(' + "json['${f.nameField}'])";
          }else{
            f.type = f.type + 'Model';
            f.extractor = f.type + '.fromJson(' + "json['${f.nameField}'])";
          }
      }

    }

    //Cria a String do arquivo
    String finalString = '';
      finalString += 'class ' + item.nameClass + ' {' + '\n';
      for (var f in item.fields) {
        finalString += '  ' + f.type + ' ' + f.nameField + ';\n';
      }

      finalString += '\n';

      //-----------------

      finalString += '  ' + item.nameClass + '   ({' + '\n';

      for (var f in item.fields) {
        finalString += '    this.' + f.nameField + ',\n';
      }

      finalString += '  });\n\n';

      //-------------------
      
      finalString += '  factory ' + item.nameClass + '.fromJson(Map<String, dynamic> json) =>  ' + item.nameClass + '(' + '\n';

      for (var f in item.fields) {
        finalString += '    ' + f.nameField + ': ' + f.extractor + ',\n';
      }

      finalString += '  );\n\n';

      

      //-------------------

      finalString += '  Map<String, dynamic> toMap() => { \n';

      for (var f in item.fields) {
        if(f.fieldsEnum.isNotEmpty){
          finalString += "      '" + f.nameField + "': " + f.nameField + 'EnumToString(' + f.nameField + ')' + ',\n';
        }else if(
          f.type != 'int' &&
          f.type != 'double' &&
          f.type != 'DateTime' &&
          f.type != 'String' &&
          f.type != 'bool' &&
          f.type != 'Enum' &&
          f.type != 'dynamic' &&
          f.type != 'var' &&
          (f.type.contains('List') == false)
          ){
            finalString += "    '" + f.nameField + "': " + f.nameField + '.toMap(),\n';
        }else{
          if(f.type.contains('List')){
            var typ = f.type.replaceAll('List<', '').replaceAll('>', '');
            if(
              typ != 'int' &&
              typ != 'double' &&
              typ != 'DateTime' &&
              typ != 'String' &&
              typ != 'bool' &&
              typ != 'Enum' &&
              typ != 'dynamic' &&
              typ != 'var'
            ){
              finalString += "    '" + f.nameField + "': " + item.nameClass + '.listToMap(${f.nameField}),\n';
            }else{
              finalString += "    '" + f.nameField + "': " + f.nameField + ',\n';
            }
          }else{
            finalString += "    '" + f.nameField + "': " + f.nameField + ',\n';
          }
        }
      }

      finalString += '  };\n\n';

      //-------------------

      finalString += '  static List<${item.nameClass}> listFromJson(List<dynamic> data) {\n';
      finalString += '    return data.map((post) => ${item.nameClass}.fromJson(post)).toList();\n';
      finalString += '  }\n\n';

      //-------------------

      finalString += '  static List<Map<String, dynamic>> listToMap(List<${item.nameClass}> data) {\n';
      finalString += '    return data.map((post) => post.toMap()).toList();\n';
      finalString += '  }\n\n';


      finalString += '}';


      //Cria o arquivo e escreve a String

      Directory current = Directory.current;

      File file = File('${current.path}/bin/output/${item.nameFileClass}'); 
      file.writeAsString(finalString);


  }

  

 // print(fileLines[fileLines.length - 1]);

  /* print(classNamesFiles);
  print(groups);  */
}




