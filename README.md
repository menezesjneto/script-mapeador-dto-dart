mapeador-dto-dart
Este é um mapeador de DTOs para model com Extractor, escrito em dart.
Com corpo incluindo fromJson, toJson, listFromJson.
22/04/2021 JNeto.
##########################################
Para funcionar corretamente a entrada deve ser um arquivo txt com os DTOS no tipo:
Linha 1) Nome1DoDTO - Nome1DoDTO // [nome do DTO 1]
Linha 2) ativo (optional) //[nome do campo]
Linha 3) Boolean // [tipo]
Linha 4) categoria //[nome do campo]
Linha 5) CategoriaTemplateDTO // [tipo]
Linha 6) criado (optional) //[nome do campo]
Linha 7) Date format: date-time // [tipo]
Linha 1) Nome2DoDTO - Nome2DoDTO // [nome do DTO 2]
Linha 2) ativo (optional) //[nome do campo]
Linha 3) Boolean // [tipo]
Linha 4) categoria //[nome do campo]
Linha 5) CategoriaTemplateDTO // [tipo]
Linha 6) criado (optional) //[nome do campo]
Linha 7) Date format: date-time // [tipo]
.
.
.
##########################################
Exemplo Arquivot dto.txt (pasta Input)
TemplateAtaDTO - TemplateAtaDTO  // NOME DO DTO
ativo (optional)
Boolean
categoria (optional)
CategoriaTemplateDTO
criado (optional)
Date format: date-time
empresa (optional)
EmpresaDTO
id (optional)
Long format: int64
nome (optional)
String
texto (optional)
String
UserDTO - UserDTOUp
id (optional)
Long format: int64
login (optional)
String
VideoAnexoDTO - VideoAnexoDTOUp
atividade (optional)
AtividadeDTO
conteudo (optional)
ConteudoDTO
criado (optional)
Date format: date-time
descricao (optional)
String
empresaAnexo (optional)
EmpresaDTO
filePath (optional)
String
id (optional)
Long format: int64
imposto (optional)
ImpostoDTO
nome
String
template (optional)
TemplateDTO
urlYoutube (optional)
String
##########################################
** Observações:

O script ainda não funciona com Enums**
